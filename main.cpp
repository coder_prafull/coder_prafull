#include <iostream>

using namespace std;

int main() {
    int endRange, startRange;
    cout << "Enter start range";
    cin>> startRange;
    cout << "Enter end range";
    cin>> endRange;

    for (int index = startRange; index < endRange; ++index) {
        if (index % 2 == 0 ){
            cout << index << " ";
        }
    }
}
